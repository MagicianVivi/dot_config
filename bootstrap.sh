#!/bin/sh

cd ~
ln -s ~/.config/xinit/.xinitrc .xinitrc
ln -s ~/.config/emacs/ .emacs.d
cp .config/.gitconfig_skeleton .gitconfig
cp .config/.gitignore_global .gitignore_global
read -p "Input email for .gitconfig : " email
echo "email = $email" >> .gitconfig
